
exports.run = async (client, message, args, level) => { // eslint-disable-line no-unused-vars

    if(message.member.hasPermission('ADMINISTRATOR') || message.member.hasPermission('KICK_MEMBERS')){
        if(message.mentions.members.first().hasPermission('ADMINISTRATOR') || message.mentions.members.first().hasPermission('BAN_MEMBERS') || message.mentions.members.first().hasPermission('KICK_MEMBERS')){
            return message.reply(`nie mogę wyrzucić tego użytkownika`);
        }
        let sayMessage = args.join(` `);
        let nameplus =message.member.displayName+": "+sayMessage;

        //rgs.reason = sayMessage;
        await message.mentions.members.first().ban({days:1,reason: nameplus})
        message.channel.send(`${message.mentions.members.first().displayName} został zbanowany!`)
        //message.channel.send(`hej adminku`);
    } else message.reply(`nie masz uprawnień!`);

};
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["b"],
    permLevel: "User"
  };
  
  exports.help = {
    name: "ban",
    category: "Miscelaneous",
    description: "xxx.",
    usage: "say [...text]"
  };
  