const Discord = require("discord.js");
const Fuse = require("fuse.js")
const inv = 'https://discord.com/api/oauth2/authorize?client_id=489377322042916885&permissions=8&scope=bot%20applications.commands'
exports.run = async (client, message, args, level) => { let embed;
// eslint-disable-line no-unused-vars
  if (message.member.roles.cache.some(role => role.name === '☢︎')) {
    return
  }
    if(message.mentions.members.first()) {
      if (message.mentions.members.first().roles.cache.some(role => role.name === '☢︎')) {
        return
      }
	    
      let image_url = message.mentions.members.first().user.avatarURL({ format: 'png', dynamic: true, size: 2048 })
    embed = new Discord.MessageEmbed()
    .setTitle(`${message.mentions.members.first().user.username}#${message.mentions.members.first().user.discriminator}`)
    .setAuthor(message.author.username, message.author.avatarURL())
    .setDescription(`[avatar url](${image_url}) || [invite bot](${inv})`)
    .setImage(image_url)
    .setColor(client.config.embed_color_h);
	    //mentioned user
    await message.channel.send(embed);
    return
      }
      if (args != '') {
        const nameSearch = args.join(` `)
        message.guild.members.fetch()
		      .then((guild) => {
//	console.log(guild)
          let membersArray = []
          const options = {
            keys: [
              "nickname",
              "username",
		"id"
            ]
          }
          guild.map(m=>{
            let mS = {nickname: m.nickname, username: m.user.username, avatarURL: m.user.avatarURL({ format: 'png', dynamic: true, size: 2048 }), discriminator: m.user.discriminator, id: m.user.id, member: m}
            membersArray.push(mS)
          })
          let fuse = new Fuse(membersArray, options)
          var result = fuse.search(nameSearch);
	  //fmember = message.guild.members.get('id', result[0].id)
	  //console.log(fmember)
          if (result[0].member){
             //console.log('prubujemu', result[0])
             if (result[0].member.roles.cache.some(role => role.name === '☢︎')) {
               return
             }
           }
	  // console.log(result[0])

          try {
            let image_url = result[0].avatarURL
            let embed = new Discord.MessageEmbed()
            .setTitle(`${result[0].username}#${result[0].discriminator}`)
            .setAuthor(message.author.username, message.author.avatarURL())
            .setDescription(`[avatar url](${image_url}) || [invite bot](${inv})`)
            .setImage(image_url)
            .setColor(client.config.embed_color_h)
            message.channel.send(embed);
            return

          } catch (err) {
		let image_url = message.author.avatarURL({ format: 'png', dynamic: true, size: 2048 })
            let embed = new Discord.MessageEmbed()
            .setTitle(`${message.author.username}#${message.author.discriminator}`)
            .setAuthor(message.author.username, message.author.avatarURL())
            .setDescription(`[avatar url](${image_url}) || [invite bot](${inv})`)
            .setImage(image_url)
            .setColor(client.config.embed_color_h)
            message.channel.send(embed);
          }
        }) 
      }
      else {
	//console.log('ja')
        let image_url = message.author.avatarURL({ format: 'png', dynamic: true, size: 2048 })
	      // console.log(message.author)

       // if (image_url.endsWith('.gif')){
         // image_url = image_url.concat('?size=2048')
       // }
        embed = new Discord.MessageEmbed()
            .setTitle(`${message.author.username}#${message.author.discriminator}`)
            .setAuthor(message.author.username, message.author.avatarURL())
            .setDescription(`[avatar url](${image_url}) || [invite bot](${inv})`)
            .setImage(image_url)
            .setColor(client.config.embed_color_h);
        await message.channel.send(embed);
        }

      }
  
  exports.conf = {
    enabled: true,
    guildOnly: false,
    aliases: ["a"],
    permLevel: "User"
  };
  
  exports.help = {
    name: "avatar",
    category: "Miscelaneous",
    description: "xxx.",
    usage: "say [...text]"
  };
  
